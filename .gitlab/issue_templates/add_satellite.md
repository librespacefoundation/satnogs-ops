### Satellite name
<!--Add the satellite name and possible alternative names -->

### Description
<!-- Short (<50 words) description about the satellite mission -->

### Image
<!-- Link or attach an image of the satellite no larger than 500x500px -->

### Transmitter 1
<!-- complete information about transmitter of the satellite. Copy and paste for 2nd transmitter and so on -->

#### Transmitter Name
<!-- Short name/description of the transmitter -->

#### Type
<!-- Select from Transmitter/Transceiver/Transponder -->

#### Service
<!-- Select transmitter service from the following:
'Aeronautical', 'Amateur', 'Broadcasting', 'Earth Exploration', 'Fixed', 'Inter-satellite',
'Maritime', 'Meteorological', 'Mobile', 'Radiolocation', 'Radionavigational',
'Space Operation', 'Space Research', 'Standard Frequency and Time Signal', 'Unknown' -->

#### Downlink Frequency
<!-- Downlink frequency in hertz -->

#### Downlink Mode
<!-- Downlink Mode and Baud rate -->

#### Dowlink Encoding
<!-- Provide encoding information and links available -->

#### Uplink Frequency
<!-- Uplink frequency in hertz -->

#### Uplink Mode
<!-- Uplink Mode and Baud rate -->

#### Uplink Encoding
<!-- Provide encoding information and links available -->


/label ~satellite ~perm::moderator